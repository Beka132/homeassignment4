package com.example.assignment3

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Patterns
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*
import java.util.regex.Pattern

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        btnSave.setOnClickListener {
            val email:String=etEmail.text.toString()
            val firstName:String=etFirstName.text.toString()
            val lastName:String=etLastName.text.toString()
            val userName:String=etUserName.text.toString()
            val age:String=etAge.text.toString()
            if (email.isNotEmpty()&&firstName.isNotEmpty()&&lastName.isNotEmpty()&&userName.isNotEmpty()&&age.isNotEmpty()){
                if (userName.length<10)Toast.makeText(this, "User Name Should be 10 Characters Long", Toast.LENGTH_SHORT).show()
                if (age.toInt()<0||etAge.text.toString().contains("."))Toast.makeText(this, "Age Should be a Positive and Natural Number", Toast.LENGTH_SHORT).show()
                if (!Patterns.EMAIL_ADDRESS.matcher(email).matches())Toast.makeText(this, "Your Email Address is not Valid", Toast.LENGTH_SHORT).show()
            }else{
                Toast.makeText(this, "Please Fill Each One of the Given Fields", Toast.LENGTH_SHORT).show()
            }
        }

        btnClear.setOnLongClickListener {
            etEmail.setText("")
            etFirstName.setText("")
            etLastName.setText("")
            etUserName.setText("")
            etAge.setText("")
            return@setOnLongClickListener true
        }
    }
}